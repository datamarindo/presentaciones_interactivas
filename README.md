# presentaciones_interactivas


material para hacer presentaciones participativas, con actividades como:

* mapa interactivo para decir de qué entidad provienen
* mapa interactivo para decir de qué municipio de Veracruz provienene
* juego tipo *cien mexicanos dijeron* (por el momento requiere de internet forzosamente, o servir la base de datos de preguntas)

Los mapas son SVG, los polígonos de estados/municipios deben llevar el parámetro `id=NOMGEO`, se pueden hacer en mapshaper pasando el argumento   `id-field=FOO` en el diálogo de exportación.

Se anexa un código de R que pone los nodos `<title>` para que se despliegue el nombre del municipio on-hover; además de poner el parámetro `onClick` y el color, para que el código lo pueda sustituir con una lista de colores nombrados.
